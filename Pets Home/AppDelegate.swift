//
//  AppDelegate.swift
//  Pets Home
//
//  Created by Gabani King on 01/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import IQKeyboardManager
import SocketIO

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var isDoctorLogin = false
    var dicLoginUserDetails = NSDictionary()
    
    var chatUserName: [String] = []
       
       let socketManager = SocketManager(socketURL: URL(string: BASE_URL_SOCKET)!, config: [.log(false), .compress])

       var socket:SocketIOClient!
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared().isEnabled = true
        
        self.dicLoginUserDetails = getCurrentUserData()
        socket = socketManager.defaultSocket
        
        if let isUserLogin = UserDefaults.standard.value(forKey: "isUserLogin") as? Bool {
            if isUserLogin == true {
                setUpCustomerLogin()
            }
        }
        
        return true
    }
    
    func addSocketHandlers() {
        socket.on(clientEvent: .connect) {data, ack in
            print("App connected to socket server")
        }
    }
    
    func saveCurrentUserData(dic: NSDictionary)
    {
        let data = NSKeyedArchiver.archivedData(withRootObject: dic)
        UserDefaults.standard.setValue(data, forKey: "LoginData")
        UserDefaults.standard.synchronize()
    }
    
    func getCurrentUserData() -> NSDictionary
    {
        if let data = UserDefaults.standard.object(forKey: "LoginData"){
            
            let arrayObjc = NSKeyedUnarchiver.unarchiveObject(with: data as! Data)
            return arrayObjc as! NSDictionary
        }
        return [:]
    }
    
    func setUpCustomerLogin() {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = homeNavigation
        appDelegate.window?.makeKeyAndVisible()
    }
}


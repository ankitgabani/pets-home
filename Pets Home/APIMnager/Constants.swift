//
//  Constants.swift
//  Nhyira Premium
//
//  Created by Gabani King on 18/01/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import Foundation
import UIKit

//MARK:- BASE_URL
let BASE_URL = "http://47.112.187.226/api/"

let BASE_URL_SOCKET = "http://47.112.187.226:5000"


//MARK:- End Point
// **************************
let LOGIN_USER = "p_user_login.php"
let FORGOT_PASSWORD = "forget.php"
let REGISTER_APP = "p_reg_user.php"
let HOME_DATA = "p_home_data.php"
let DOCTOR_LIST = "doctor_list.php"
let HISTORY_CHAT = "history_chat.php"
let PET_SEARCH = "pet_search.php"
let UPDATE_PROFILE = "update_profile.php"
let REVIEW_LIST = "review_list.php"
let FEEDBACK = "feedback.php"
let ARTICLE_LIST = "article_list.php"
let UPDATE_IMAGE = "update_image.php"
let ADD_ARTICLE = "add_article.php"
let ADD_PET_INFOMARTION = "add_pet_information.php"
let CHANGE = "change.php"
let CONV_MESSAGE = "conv_message.php"
let PET_CATEGORY = "pet_category.php"

let appDelegate = UIApplication.shared.delegate as! AppDelegate

//
//  CCertificationViewController.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import SVProgressHUD

class CCertificationViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgVerify: UIImageView!
    @IBOutlet weak var lblStarCount: UILabel!
    @IBOutlet weak var txtMessae: UITextView!
    
    @IBOutlet weak var lblName: UILabel!
    var dicPetDetails = NSDictionary()
    var isFromDr = false
    
    var isUploadFile = false
    var selectedImage = UIImage()
    
    let Imgpicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Imgpicker.delegate = self
        
        if isFromDr == true {
            let username = dicPetDetails.value(forKey: "doctor_name") as? String
            self.lblName.text = username
            
            let rate = dicPetDetails.value(forKey: "rate") as? Int
            self.lblStarCount.text = "\(rate ?? 0)"
            
            let is_verified = dicPetDetails.value(forKey: "is_verified") as? String
            
            if is_verified == "1" {
                imgVerify.isHidden = false
            }
            else {
                imgVerify.isHidden = true
            }
            
            if var image1 = (dicPetDetails.value(forKey: "doctor_pic") as? String) {
                image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url1 = URL(string: "http://47.112.187.226/\(image1)")
                imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
            }
            else {
                imgProfile.image = UIImage(named: "ic_placeholder")
            }
        }
        else {
            let username = dicPetDetails.value(forKey: "username") as? String
            self.lblName.text = username
            
            let rate = dicPetDetails.value(forKey: "rate") as? Int
            self.lblStarCount.text = "\(rate ?? 0)"
            
            let is_verified = dicPetDetails.value(forKey: "is_verified") as? Int
            
            if is_verified == 1{
                imgVerify.isHidden = false
            }
            else {
                imgVerify.isHidden = true
            }
            
            if var image1 = (dicPetDetails.value(forKey: "profile_pic") as? String) {
                image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let url1 = URL(string: "http://47.112.187.226/\(image1)")
                imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
            }
            else {
                imgProfile.image = UIImage(named: "ic_placeholder")
            }
        }
        
        
        
        self.tabBarController?.tabBar.isHidden = true
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func fileclikcedAttched(_ sender: Any) {
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        if self.txtMessae.text == "" {
            self.view.hideToast()
            self.view.makeToast("Enter Message")
        }
        else if isUploadFile == true
        {
            self.view.hideToast()
            
            let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
            
            let param = ["uid": "\(user_Id ?? "")","msg":self.txtMessae.text!,"size":"1","image0": ""]
            
            uploadPhoto("http://47.112.187.226/api/add_article.php", image: selectedImage, params: param, header: [:]) {
            }
        }
        else {
            self.view.hideToast()
            callAddMessageAPI()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            
            self.selectedImage = pickedImage
            self.isUploadFile = true
        }
        
        picker.dismiss(animated:true,completion:nil)
    }
    
    
    func callAddMessageAPI() {
        
        APIClient.sharedInstance.showIndicator()
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let param = ["uid": "\(user_Id ?? "")","msg":self.txtMessae.text!,"size":"1","image0": ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithOutHeaderPost(ADD_ARTICLE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["ResponseMsg"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        let strMsg = responseUser.value(forKey: "ResponseMsg") as? String
                        let status = responseUser.value(forKey: "Result") as? String
                        
                        if status == "true" {
                            
                            self.txtMessae.text = ""
                            self.view.makeToast(strMsg)
                        }
                        else {
                            self.view.makeToast(strMsg)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        SVProgressHUD.show()
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "image0", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "ResponseMsg") as? String)
            print(responseDict?.value(forKey: "ResponseMsg") as? String ?? "")
            self.txtMessae.text = ""
            self.isUploadFile = false
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                SVProgressHUD.dismiss()
                print("upload success result: \(resut)")
            case .failure(let err):
                SVProgressHUD.dismiss()
                print("upload err: \(err)")
            }
        }
    }
}

//
//  CHomeCollectionReusableView.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CHomeCollectionReusableView: UICollectionReusableView {
        @IBOutlet weak var btnCat: UIButton!
               @IBOutlet weak var btnDog: UIButton!
               @IBOutlet weak var viewCat: UIView!
               @IBOutlet weak var viewDog: UIView!
}

//
//  CPetDetailsVC.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class CPetDetailsVC: UIViewController {
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblPetTitle: UILabel!
    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var imgMiddlePet: UIImageView!
    
    @IBOutlet weak var lbldes: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblOewnr: UILabel!
    
    
    @IBOutlet weak var imgProPic: UIImageView!
    
    var dicPetDetails = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        setUp()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    func setUp() {
        
        let username = dicPetDetails.value(forKey: "username") as? String
        let pet_title = dicPetDetails.value(forKey: "pet_title") as? String
        let pet_gender = dicPetDetails.value(forKey: "pet_gender") as? String
        let pet_location = dicPetDetails.value(forKey: "pet_location") as? String
        let pet_description = dicPetDetails.value(forKey: "pet_description") as? String

        lblUserName.text = username
        lbldes.text = pet_description
        lblLocation.text = pet_location
        lblGender.text = pet_gender
        lblPetTitle.text = pet_title
        
        let arrPet = dicPetDetails.value(forKey: "pet_image") as? NSArray
        var image = arrPet![0] as? String
        image = image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: "http://47.112.187.226/\(image!)")
        imgPet.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgPet.sd_setImage(with: url, placeholderImage: UIImage(named: "logo"))
        
        if var image1 = (dicPetDetails.value(forKey: "profile_pic") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            imgProPic.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProPic.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            imgProPic.image = UIImage(named: "ic_placeholder")
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedCertification(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CCertificationViewController = mainStoryboard.instantiateViewController(withIdentifier: "CCertificationViewController") as! CCertificationViewController
        vc.dicPetDetails = self.dicPetDetails
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

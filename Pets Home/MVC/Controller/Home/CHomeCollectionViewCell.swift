//
//  CHomeCollectionViewCell.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class CHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblOwner: UILabel!
    
    
}

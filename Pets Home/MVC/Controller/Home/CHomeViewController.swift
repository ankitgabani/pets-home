//
//  CHomeViewController.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import Alamofire
import SVProgressHUD

class CHomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,responseDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var searchBar: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tblViewAddPet: UITableView!
    
    @IBOutlet weak var viewTargetCat: UIView!
    @IBOutlet weak var txtPetCat: UITextField!
    
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtAge: UITextField!
    @IBOutlet weak var txtDes: UITextView!
    @IBOutlet weak var txtLocation: UITextField!
    
    
    @IBOutlet weak var btnName: UILabel!

    
    
    let sectionInsets = UIEdgeInsets(top: 0.0,
                                     left: 8.0,
                                     bottom: 8.0,
                                     right: 8.0)
    let itemsPerRow: CGFloat = 2
    
    var flowLayout: UICollectionViewFlowLayout {
        let _flowLayout = UICollectionViewFlowLayout()
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        _flowLayout.itemSize = CGSize(width: widthPerItem, height: 212)
        
        _flowLayout.sectionInset = UIEdgeInsets(top: 0, left: 8, bottom: 8, right: 8)
        _flowLayout.scrollDirection = UICollectionView.ScrollDirection.vertical
        _flowLayout.minimumInteritemSpacing = 0.0
        _flowLayout.minimumLineSpacing = 10.0
        // edit properties here
        
        return _flowLayout
    }
    
    let dropDownGender = DropDown()
    var arrayGender = ["公","母"]
    
    let dropDownPetCat = DropDown()
    var arrayPetCat = ["猫","狗"]
    
    
    var isSelectedCat = true
    
    var arrCarData = NSMutableArray()
    
    var isUploadFile = false
    var selectedImage = UIImage()
    
    let Imgpicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        Imgpicker.delegate = self
        callHomeDataAPI(status: "猫")
        self.tblViewAddPet.isHidden = true
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.collectionView.collectionViewLayout = flowLayout
        setDropDownGender()
        setDropDownCat()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func setDropDownGender() {
        dropDownGender.dataSource = arrayGender
        dropDownGender.anchorView = viewGender
        dropDownGender.direction = .any
        dropDownGender.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtGender.text = item
        }
        dropDownGender.bottomOffset = CGPoint(x: 0, y: viewGender.bounds.height)
        dropDownGender.topOffset = CGPoint(x: 0, y: -viewGender.bounds.height)
        dropDownGender.dismissMode = .onTap
        dropDownGender.textColor = UIColor.darkGray
        dropDownGender.backgroundColor = UIColor.white
        dropDownGender.selectionBackgroundColor = UIColor.clear
        dropDownGender.reloadAllComponents()
    }
    
    func setDropDownCat() {
        dropDownPetCat.dataSource = arrayPetCat
        dropDownPetCat.anchorView = viewTargetCat
        dropDownPetCat.direction = .any
        dropDownPetCat.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtPetCat.text = item
        }
        dropDownPetCat.bottomOffset = CGPoint(x: 0, y: viewTargetCat.bounds.height)
        dropDownPetCat.topOffset = CGPoint(x: 0, y: -viewTargetCat.bounds.height)
        dropDownPetCat.dismissMode = .onTap
        dropDownPetCat.textColor = UIColor.darkGray
        dropDownPetCat.backgroundColor = UIColor.white
        dropDownPetCat.selectionBackgroundColor = UIColor.clear
        dropDownPetCat.reloadAllComponents()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCarData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CHomeCollectionViewCell", for: indexPath) as! CHomeCollectionViewCell
        
        let dicPetData = arrCarData[indexPath.row] as? NSDictionary
        
        let username = dicPetData?.value(forKey: "username") as? String
        
        cell.lblUsername.text = username
        cell.lblOwner.text = "发布者"
        
        let arrPet = dicPetData?.value(forKey: "pet_image") as? NSArray
        
        var image = arrPet![0] as? String
        image = image?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = URL(string: "http://47.112.187.226/\(image!)")
        cell.imgPet.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.imgPet.sd_setImage(with: url, placeholderImage: UIImage(named: "logo"))
        
        if var image1 = (dicPetData?.value(forKey: "profile_pic") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgPro.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            cell.imgPro.image = UIImage(named: "ic_placeholder")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dicPetData = arrCarData[indexPath.row] as? NSDictionary
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CPetDetailsVC = mainStoryboard.instantiateViewController(withIdentifier: "CPetDetailsVC") as! CPetDetailsVC
        vc.dicPetDetails = dicPetData!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "CHomeCollectionReusableView", for: indexPath) as! CHomeCollectionReusableView
        
        headerView.btnCat.addTarget(self, action: #selector(clickedOnCat), for: .touchUpInside)
        
        headerView.btnDog.addTarget(self, action: #selector(clickedOnDog), for: .touchUpInside)
        
        if isSelectedCat == true {
            headerView.viewCat.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            headerView.btnCat.setTitleColor(UIColor.white, for: .normal)
            
            headerView.viewDog.backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 234/255, alpha: 1)
            headerView.btnDog.setTitleColor(UIColor.black, for: .normal)
            
        } else {
            headerView.viewDog.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            headerView.btnDog.setTitleColor(UIColor.white, for: .normal)
            
            headerView.viewCat.backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 234/255, alpha: 1)
            headerView.btnCat.setTitleColor(UIColor.black, for: .normal)
            
        }
        
        return headerView
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: collectionView.frame.size.width, height: 130)
    }
    
    @objc func clickedOnCat() {
        callHomeDataAPI(status: "猫")
        isSelectedCat = true
        self.collectionView.reloadData()
    }
    
    @objc func clickedOnDog() {
        callHomeDataAPI(status: "狗")
        isSelectedCat = false
        self.collectionView.reloadData()
    }
    
    
    @IBAction func clickedSearchPet(_ sender: Any) {
        callSearchPetAPI(keyword: self.searchBar.text!)
    }
    
    @IBAction func clickedAttachedFile(_ sender: Any) {
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    @IBAction func clickedChoosePetCat(_ sender: Any) {
        dropDownPetCat.show()
    }
    
    @IBAction func clickedGender(_ sender: Any) {
        dropDownGender.show()
    }
    
    @IBAction func clickedCancel(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        self.tblViewAddPet.isHidden = true
    }
    
    @IBAction func clickedAdd(_ sender: Any) {
        self.view.hideToast()

        if self.txtName.text == "" {
            self.view.makeToast("Enter pet Name")
            
        } else if self.txtAge.text == "" {
            self.view.makeToast("Enter pet Age")

        } else if self.txtDes.text == "" {
            self.view.makeToast("Enter pet description")

        } else if self.txtLocation.text == "" {
            self.view.makeToast("Enter pet location")
            
        } else if isUploadFile == false {
            
            self.view.makeToast("select pet image")
            
//            let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
//
//            let param = ["uid": "\(user_Id ?? "")","pet_cat": "1","Age": self.txtAge.text!,"size":"1","image0": "","pet_title": self.txtName.text!,"pet_gender": self.txtGender.text!,"pet_description": self.txtDes.text!,"pet_location": self.txtLocation.text!]
//
//            uploadPhoto("http://47.112.187.226/api/add_pet_information.php", image: selectedImage, params: param, header: [:]) {
//
//            }
            
        } else {
            
            let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
            
            let param = ["uid": "\(user_Id ?? "")","pet_cat": "1","Age": self.txtAge.text!,"size":"1","image0": "","pet_title": self.txtName.text!,"pet_gender": self.txtGender.text!,"pet_description": self.txtDes.text!,"pet_location": self.txtLocation.text!]
            
            uploadPhoto("http://47.112.187.226/api/add_pet_information.php", image: selectedImage, params: param, header: [:]) {
                
            }
            
            
            //callAddPetsAPI()
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            
            self.selectedImage = pickedImage
            self.isUploadFile = true
        }
        
        picker.dismiss(animated:true,completion:nil)
    }
    
    func callAddPetsAPI() {
        
        APIClient.sharedInstance.showIndicator()
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let param = ["uid": "\(user_Id ?? "")","pet_cat": "1","Age": self.txtAge.text!,"pet_title": self.txtName.text!,"pet_gender": self.txtGender.text!,"pet_description": self.txtDes.text!,"pet_location": self.txtLocation.text!,"size":"1","image0": ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithOutHeaderPost(ADD_PET_INFOMARTION, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["ResponseMsg"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        let strMsg = responseUser.value(forKey: "ResponseMsg") as? String
                        let status = responseUser.value(forKey: "Result") as? String
                        
                        if status == "true" {
                            
                            self.txtAge.text = ""
                            self.txtDes.text = ""
                            self.txtName.text = ""
                            self.txtLocation.text = ""
                            self.view.makeToast(strMsg)
                        }
                        else {
                            self.view.makeToast(strMsg)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        SVProgressHUD.show()
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "image0", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "ResponseMsg") as? String)
            print(responseDict?.value(forKey: "ResponseMsg") as? String ?? "")
            self.txtAge.text = ""
            self.txtDes.text = ""
            self.txtName.text = ""
            self.txtLocation.text = ""
            self.isUploadFile = false
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                SVProgressHUD.dismiss()
                print("upload success result: \(resut)")
            case .failure(let err):
                SVProgressHUD.dismiss()
                print("upload err: \(err)")
            }
        }
    }
    
    @IBAction func clickedAddNewAPet(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = true
        self.tblViewAddPet.isHidden = false
    }
    
    
    @IBAction func clickedHomeTab(_ sender: Any) {
    }
    
    @IBAction func clickedDrTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: TopPetDoctorVC = mainStoryboard.instantiateViewController(withIdentifier: "TopPetDoctorVC") as! TopPetDoctorVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedServiceTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ArticleViewController = mainStoryboard.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ChattingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChattingsViewController") as! ChattingsViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedAccountTab(_ sender: Any) {
        
        let type = appDelegate.dicLoginUserDetails.value(forKey: "type") as? String
        
        if type == "医生" {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: DAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "DAccountVC") as! DAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: CAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "CAccountVC") as! CAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    
    func callHomeDataAPI(status: String) {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        dic.setValue(status, forKey: "status")
        WebParserWS.fetchDataWithURL(url: BASE_URL + HOME_DATA as NSString, type: .TYPE_POST_RAWDATA, ServiceName: HOME_DATA, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func callSearchPetAPI(keyword: String) {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        dic.setValue(keyword, forKey: "keyword")
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + PET_SEARCH as NSString, type: .TYPE_POST_RAWDATA, ServiceName: PET_SEARCH, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            if result == "true" {
                
                let arrPetlist = dicResponse?.value(forKey: "petlist") as? NSArray
                
                self.arrCarData.removeAllObjects()
                self.arrCarData = (arrPetlist?.mutableCopy() as? NSMutableArray)!
                self.collectionView.reloadData()
            }
            
        }
    }
}

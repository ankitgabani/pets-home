//
//  IntoViewController.swift
//  Pets Home
//
//  Created by Gabani King on 02/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class IntoViewController: UIViewController {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func clickedSkip(_ sender: Any) {
        setUpCustomerLogin()
    }
    
    func setUpCustomerLogin() {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        appDelegate.window?.rootViewController = homeNavigation
        appDelegate.window?.makeKeyAndVisible()
    }
}

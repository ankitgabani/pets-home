//
//  ChattingsViewController.swift
//  Pets Home
//
//  Created by Gabani King on 08/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ChattingsViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedDrTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: TopPetDoctorVC = mainStoryboard.instantiateViewController(withIdentifier: "TopPetDoctorVC") as! TopPetDoctorVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedServiceTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ArticleViewController = mainStoryboard.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        
    }
    
    @IBAction func clickedAccountTab(_ sender: Any) {
        
        let type = appDelegate.dicLoginUserDetails.value(forKey: "type") as? String
        
        if type == "医生" {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: DAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "DAccountVC") as! DAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: CAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "CAccountVC") as! CAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
}

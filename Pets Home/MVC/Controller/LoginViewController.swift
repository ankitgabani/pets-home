//
//  LoginViewController.swift
//  Pets Home
//
//  Created by Gabani King on 02/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Toast_Swift
class LoginViewController: UIViewController,responseDelegate {
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var viewPopup: UIView!
    
    @IBOutlet weak var mainViewPopup: UIView!
    
    @IBOutlet weak var txtForgotEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainViewPopup.isHidden = true
        
        viewPopup.clipsToBounds = true
        viewPopup.layer.cornerRadius = 25
        viewPopup.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively
        
        viewPopup.slideOut(to: kFTAnimationBottom, in: viewPopup.superview, duration: 0.0, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
//        txtEmail.text = "mayur1@gmail.com"
//        txtPassword.text = "123"
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        mainViewPopup.addGestureRecognizer(tap)

        //        txtEmail.text = "gabani7004@gmail.com"
        //        txtPassword.text = "12345678"
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedSubmitForgot(_ sender: Any) {
        
        self.mainViewPopup.isHidden = true
        self.viewPopup.slideOut(to: kFTAnimationBottom, in: self.viewPopup.superview, duration: 0.0, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        callForgotPassAPI()
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.mainViewPopup.isHidden = true
         viewPopup.slideOut(to: kFTAnimationBottom, in: viewPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    
    @IBAction func clickedLogin(_ sender: Any) {
        
        callLoginAPI()
    }
    
    @IBAction func clickedSignUp(_ sender: Any) {
        
    }
    
    @IBAction func clickedFotPass(_ sender: Any) {
        mainViewPopup.isHidden = false
        viewPopup.slideIn(from: kFTAnimationBottom, in: viewPopup.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    
    @IBAction func clickedSkip(_ sender: Any) {
        appDelegate.setUpCustomerLogin()
    }
    
    func callLoginAPI() {
        let dic = NSMutableDictionary()
        dic.setValue(txtPassword.text!, forKey: "password")
        dic.setValue(txtEmail.text!, forKey: "username_email")
        WebParserWS.fetchDataWithURL(url: BASE_URL + LOGIN_USER as NSString, type: .TYPE_POST_RAWDATA, ServiceName: LOGIN_USER, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func callForgotPassAPI() {
        let dic = NSMutableDictionary()
        dic.setValue(txtForgotEmail.text!, forKey: "mobile")
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + FORGOT_PASSWORD as NSString, type: .TYPE_POST_RAWDATA, ServiceName: FORGOT_PASSWORD, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            self.view.makeToast(responseMsg)
            
            if result == "true" {
                
                if ServiceName == LOGIN_USER {
                    let dicUserData = dicResponse?.value(forKey: "UserLogin") as? NSDictionary
                    
                    let isDoctor = dicUserData?.value(forKey: "type") as? String
                    
                    if isDoctor == "医生" {
                        UserDefaults.standard.set(true, forKey: "isDoctor")
                    }
                    else {
                        UserDefaults.standard.set(false, forKey: "isDoctor")
                    }
                    
                    UserDefaults.standard.set(true, forKey: "isUserLogin")
                    UserDefaults.standard.synchronize()
                    
                    appDelegate.saveCurrentUserData(dic: dicUserData!)
                    appDelegate.dicLoginUserDetails = dicUserData!
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        appDelegate.setUpCustomerLogin()
                    }
                }
                else {
                    
                }
                
            }
        }
    }
}

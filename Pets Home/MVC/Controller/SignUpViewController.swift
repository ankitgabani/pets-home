//
//  SignUpViewController.swift
//  Pets Home
//
//  Created by Gabani King on 02/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown

class SignUpViewController: UIViewController,responseDelegate {
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var viewOwner: UIView!
    @IBOutlet weak var imgOwner: UIImageView!
    @IBOutlet weak var lblOwner: UILabel!
    
    @IBOutlet weak var viewDoctor: UIView!
    @IBOutlet weak var imgDoctor: UIImageView!
    @IBOutlet weak var lblDoctor: UILabel!
    
    @IBOutlet weak var mainViewHeightCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewDoctorType: UIView!
    @IBOutlet weak var viewDTypeHeightCont: NSLayoutConstraint!
    @IBOutlet weak var viewTargteCont: NSLayoutConstraint!
    
    @IBOutlet weak var viewTarget: UIView!
    @IBOutlet weak var txtType: UITextField!
    
    @IBOutlet weak var viewMain: UIView!
    
    let dropDown = DropDown()
    var arrayType = ["皮肤科","内科"]

    var isOwner = "所有者"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DispatchQueue.main.async {
            self.mainViewHeightCont.constant = 870
            self.viewDoctorType.isHidden = true
            self.viewDTypeHeightCont.constant = 0
            self.viewTargteCont.constant = 0
        }
        
        viewOwner.borderColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        lblOwner.textColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        imgOwner.image = UIImage(named: "ic_blue_cat")
        
        viewDoctor.borderColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        lblDoctor.textColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        imgDoctor.image = UIImage(named: "ic_black_doctor")
        setdropDown()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedOwner(_ sender: Any) {
        isOwner = "所有者"
        viewOwner.borderColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        lblOwner.textColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        imgOwner.image = UIImage(named: "ic_blue_cat")
        
        viewDoctor.borderColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        lblDoctor.textColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        imgDoctor.image = UIImage(named: "ic_black_doctor")
        
        mainViewHeightCont.constant = 870
            self.viewDoctorType.isHidden = true
            self.viewDTypeHeightCont.constant = 0
            self.viewTargteCont.constant = 0
        
    }
    
    @IBAction func clickedDoctor(_ sender: Any) {
         isOwner = "医生"
        viewDoctor.borderColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        lblDoctor.textColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        imgDoctor.image = UIImage(named: "ic_blue_doctor")
        
        viewOwner.borderColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        lblOwner.textColor = UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1)
        imgOwner.image = UIImage(named: "ic_black_cat")
        
        mainViewHeightCont.constant = 930
            self.viewDoctorType.isHidden = false
            self.viewDTypeHeightCont.constant = 92
            self.viewTargteCont.constant = 1
    }
    
    @IBAction func clickedSignUp(_ sender: Any) {
        callRegisterAPI()
    }
    
    func setdropDown() {
        dropDown.dataSource = arrayType
        dropDown.anchorView = viewTarget
        dropDown.direction = .any
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtType.text = item
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    @IBAction func clickedDrType(_ sender: Any) {
        dropDown.show()
    }
    
    func callRegisterAPI() {
        let dic = NSMutableDictionary()
        dic.setValue(txtName.text!, forKey: "username")
        dic.setValue(txtEmail.text!, forKey: "email")
        dic.setValue(txtEmail.text!, forKey: "mobile")
        dic.setValue(txtPassword.text!, forKey: "password")
        dic.setValue(isOwner, forKey: "type")
        if isOwner == "医生" {
            dic.setValue("内科", forKey: "skill")
        }
        else {
            dic.setValue("", forKey: "skill")
        }

        WebParserWS.fetchDataWithURL(url: BASE_URL + REGISTER_APP as NSString, type: .TYPE_POST_RAWDATA, ServiceName: REGISTER_APP, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject) {
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            self.view.makeToast(responseMsg)

            if ServiceName == REGISTER_APP {
                if result == "true" {
                    self.callLoginAPI()
                }
            }
            else {
                
                if result == "true" {
                    let dicUserData = dicResponse?.value(forKey: "UserLogin") as? NSDictionary
                    appDelegate.saveCurrentUserData(dic: dicUserData!)
                    appDelegate.dicLoginUserDetails = dicUserData!
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                        appDelegate.setUpCustomerLogin()
                    }
                }
            }
        }
    }
    
    func callLoginAPI() {
        let dic = NSMutableDictionary()
        dic.setValue(txtPassword.text!, forKey: "password")
        dic.setValue(txtEmail.text!, forKey: "username_email")
        WebParserWS.fetchDataWithURL(url: BASE_URL + LOGIN_USER as NSString, type: .TYPE_POST_RAWDATA, ServiceName: LOGIN_USER, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
}

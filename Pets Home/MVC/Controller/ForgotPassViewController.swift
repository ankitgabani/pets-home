//
//  ForgotPassViewController.swift
//  Pets Home
//
//  Created by Gabani King on 02/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ForgotPassViewController: UIViewController,responseDelegate {
    
    @IBOutlet weak var txtPhone: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func clickedSend(_ sender: Any) {
        callForgotPassAPI()
    }
    
    func callForgotPassAPI() {
        let dic = NSMutableDictionary()
        dic.setValue(txtPhone.text!, forKey: "mobile")
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + FORGOT_PASSWORD as NSString, type: .TYPE_POST_RAWDATA, ServiceName: FORGOT_PASSWORD, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            self.view.makeToast(responseMsg)
            
            if result == "true" {
                
            }
        }
    }
    
}

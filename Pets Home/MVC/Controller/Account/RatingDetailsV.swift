//
//  RatingDetailsV.swift
//  Pets Home
//
//  Created by Gabani King on 25/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class RatingDetailsV: UIViewController,UITableViewDelegate, UITableViewDataSource,responseDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var clickedBack: NSLayoutConstraint!
    
    var arrRateList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        self.tabBarController?.tabBar.isHidden = true
        
        callRateListDataAPI()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRateList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell") as! RatingCell
        
        let dicData = arrRateList[indexPath.row] as? NSDictionary
        
        let name = dicData?.value(forKey: "username") as? String
        let skill = dicData?.value(forKey: "comment") as? String
        let review = dicData?.value(forKey: "review") as? String

        cell.lblSub.text = skill
        cell.lblName.text = name
        cell.lblRate.text = review
        
        if var image1 = (dicData?.value(forKey: "profile_pic") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            cell.imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgPro.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            cell.imgPro.image = UIImage(named: "ic_placeholder")
        }
        
        
        return cell
    }
     
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callRateListDataAPI() {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue("2", forKey: "uid")
        
        print(dic)
        WebParserWS.fetchDataWithURL(url: BASE_URL + REVIEW_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: REVIEW_LIST, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            if ServiceName == REVIEW_LIST {
                let arrPetlist = dicResponse?.value(forKey: "ReviewData") as? NSArray
                
                self.arrRateList.removeAllObjects()
                self.arrRateList = (arrPetlist?.mutableCopy() as? NSMutableArray)!
                self.tblView.reloadData()
            }
            
        }
    }
}

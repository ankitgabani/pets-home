//
//  OtherImageCell.swift
//  宠物之家
//
//  Created by Gabani King on 11/03/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class OtherImageCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var imgMessage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  RatingCell.swift
//  Pets Home
//
//  Created by Gabani King on 25/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class RatingCell: UITableViewCell {

    @IBOutlet weak var imgPro: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSub: UILabel!
    @IBOutlet weak var lblRate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

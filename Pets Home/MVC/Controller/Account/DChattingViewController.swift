//
//  ChattingViewController.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class DChattingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, responseDelegate {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    var objCov_id = String()
    var objName = String()
    var objOtherProfilePic: String?
    
    var arrChatHistory = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblView.delegate = self
        tblView.dataSource = self
        
        self.lblTitle.text = objName
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        
        callChatListDataAPI()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChatHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dicData = arrChatHistory[indexPath.row] as? NSDictionary
        let username = appDelegate.dicLoginUserDetails.value(forKey: "username") as? String
        let Owner_profile_pic = appDelegate.dicLoginUserDetails.value(forKey: "profile_pic") as? String

        let objUserName = dicData?.value(forKey: "username") as? String
        let file = dicData?.value(forKey: "file") as? String
        
        let message = dicData?.value(forKey: "message") as? String
        let date = dicData?.value(forKey: "date") as? String
        
        
        if username == objUserName {
            
            if file == "0" {
                let cell = tblView.dequeueReusableCell(withIdentifier: "OwnerTextCell") as! OwnerTextCell
                
                cell.lblMsg.text = message
                cell.lblDate.text = date
                
                if var image1 = Owner_profile_pic {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
                }
                else {
                    cell.imgProfile.image = UIImage(named: "ic_placeholder")
                }
                
                return cell
                
            }
            else {
                let cell = tblView.dequeueReusableCell(withIdentifier: "OwnerImageCell") as! OwnerImageCell
                cell.lblMsg.text = message
                cell.lblDate.text = date
                
                if var image1 = Owner_profile_pic {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
                }
                else {
                    cell.imgProfile.image = UIImage(named: "ic_placeholder")
                }
                
                if var image1 = file {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgMessage.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgMessage.sd_setImage(with: url1, placeholderImage: UIImage(named: "default_user"))
                }
                else {
                    cell.imgMessage.image = UIImage(named: "default_user")
                }
                return cell
                
            }
            
        }
        else {
            
            if file == "0" {
                let cell = tblView.dequeueReusableCell(withIdentifier: "OtherUserTextTableCell") as! OtherUserTextTableCell
                cell.lblMsg.text = message
                cell.lblDate.text = date
                
                if var image1 = objOtherProfilePic {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
                }
                else {
                    cell.imgProfile.image = UIImage(named: "ic_placeholder")
                }
                return cell
                
            }
            else {
                let cell = tblView.dequeueReusableCell(withIdentifier: "OtherImageCell") as! OtherImageCell
                cell.lblMsg.text = message
                cell.lblDate.text = date
                
                if var image1 = objOtherProfilePic {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
                }
                else {
                    cell.imgProfile.image = UIImage(named: "ic_placeholder")
                }
                
                if var image1 = file {
                    image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                    let url1 = URL(string: "http://47.112.187.226/\(image1)")
                    cell.imgMessage.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    cell.imgMessage.sd_setImage(with: url1, placeholderImage: UIImage(named: "default_user"))
                }
                else {
                    cell.imgMessage.image = UIImage(named: "default_user")
                }
                return cell
                
            }
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func callChatListDataAPI() {
        
        let dic = NSMutableDictionary()
        dic.setValue(objCov_id, forKey: "cov_id")
        
        print(dic)
        WebParserWS.fetchDataWithURL(url: BASE_URL + CONV_MESSAGE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: CONV_MESSAGE, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            if ServiceName == CONV_MESSAGE {
                let arrPetlist = dicResponse?.value(forKey: "CoversionData") as? NSArray
                
                self.arrChatHistory.removeAllObjects()
                self.arrChatHistory = (arrPetlist?.mutableCopy() as? NSMutableArray)!
                self.tblView.reloadData()
            }
            
        }
    }
}

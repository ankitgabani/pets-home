//
//  CAccountVC.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

protocol PizzaDelegate
{
    func onPizzaReady(type: String)
}

class CAccountVC: UIViewController,PizzaDelegate {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSatus: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tabBarController?.tabBar.isHidden = true
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let username = appDelegate.dicLoginUserDetails.value(forKey: "username") as? String
        let type = appDelegate.dicLoginUserDetails.value(forKey: "type") as? String
        let profile_pic = appDelegate.dicLoginUserDetails.value(forKey: "profile_pic") as? String
        
        lblName.text = username
        lblSatus.text = type
        if var image1 = profile_pic {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            imgProfile.image = UIImage(named: "ic_placeholder")
        }
        
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func onPizzaReady(type: String) {
        self.view.makeToast(type)
    }
    
    @IBAction func clickedProfile(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CProfileEditVC = mainStoryboard.instantiateViewController(withIdentifier: "CProfileEditVC") as! CProfileEditVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedRating(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: RatingDetailsV = mainStoryboard.instantiateViewController(withIdentifier: "RatingDetailsV") as! RatingDetailsV
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedChat(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DChatHistoryVC = mainStoryboard.instantiateViewController(withIdentifier: "DChatHistoryVC") as! DChatHistoryVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedFeedback(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DFeedbackVC = mainStoryboard.instantiateViewController(withIdentifier: "DFeedbackVC") as! DFeedbackVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedRegulation(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DRegulationVC = mainStoryboard.instantiateViewController(withIdentifier: "DRegulationVC") as! DRegulationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedService(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DSearviceVC = mainStoryboard.instantiateViewController(withIdentifier: "DSearviceVC") as! DSearviceVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedLegal(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: DLegalVC = mainStoryboard.instantiateViewController(withIdentifier: "DLegalVC") as! DLegalVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func clickedLogout(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isUserLogin")
        UserDefaults.standard.synchronize()
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home: LoginViewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        let homeNavigation = UINavigationController(rootViewController: home)
        homeNavigation.navigationBar.isHidden = true
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.window?.rootViewController = homeNavigation
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedDrTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: TopPetDoctorVC = mainStoryboard.instantiateViewController(withIdentifier: "TopPetDoctorVC") as! TopPetDoctorVC
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedServiceTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ArticleViewController = mainStoryboard.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ChattingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChattingsViewController") as! ChattingsViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedAccountTab(_ sender: Any) {
    }
    
}

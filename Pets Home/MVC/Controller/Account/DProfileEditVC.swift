//
//  DProfileEditVC.swift
//  Pets Home
//
//  Created by Gabani King on 05/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import Alamofire
import SVProgressHUD

class DProfileEditVC: UIViewController,responseDelegate,UIImagePickerControllerDelegate, UINavigationControllerDelegate  {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblNAme: UILabel!
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtemail: UITextField!
    
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var txtType: UITextField!
    
    @IBOutlet weak var viewTarget: UIView!
    
    let dropDown = DropDown()
    var arrayType = ["皮肤科","内科"]
    let Imgpicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        Imgpicker.delegate = self

        let username = appDelegate.dicLoginUserDetails.value(forKey: "username") as? String
        let email = appDelegate.dicLoginUserDetails.value(forKey: "email") as? String
        let password = appDelegate.dicLoginUserDetails.value(forKey: "password") as? String
        let mobile = appDelegate.dicLoginUserDetails.value(forKey: "mobile") as? String
        let status = appDelegate.dicLoginUserDetails.value(forKey: "status") as? String

        txtName.text = username
        lblNAme.text = username
        txtemail.text = email
        txtPassword.text = password
        txtMobile.text = mobile

        if status == "1" {
            txtType.text = "皮肤科"
        }
        else {
            txtType.text = "内科"
        }
        
        let profile_pic = appDelegate.dicLoginUserDetails.value(forKey: "profile_pic") as? String
        if var image1 = profile_pic {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgProfile.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            imgProfile.image = UIImage(named: "ic_placeholder")
        }
        
        self.tabBarController?.tabBar.isHidden = true
        setdropDown()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func setdropDown() {
        dropDown.dataSource = arrayType
        dropDown.anchorView = viewTarget
        dropDown.direction = .any
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtType.text = item
        }
        dropDown.bottomOffset = CGPoint(x: 0, y: viewTarget.bounds.height)
        dropDown.topOffset = CGPoint(x: 0, y: -viewTarget.bounds.height)
        dropDown.dismissMode = .onTap
        dropDown.textColor = UIColor.darkGray
        dropDown.backgroundColor = UIColor.white
        dropDown.selectionBackgroundColor = UIColor.clear
        dropDown.reloadAllComponents()
    }
    
    private func openGallery()
    {
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    @IBAction func clickedChoosePic(_ sender: Any) {
        openGallery()
    }
    
    @IBAction func clickedType(_ sender: Any) {
        dropDown.show()
    }
    
    func startUploadImage() {

    }
    
    @IBAction func clickedSumbit(_ sender: Any) {
        callUpdateProfileAPI()
    }
    
    @IBAction func clicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            imgProfile.contentMode = .scaleAspectFill
            
            let url = info[UIImagePickerController.InfoKey.imageURL] as? URL
            let fileName = url!.lastPathComponent

            imgProfile.image = pickedImage
            
            let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String

            let parm = ["uid":user_Id ?? "","size":"1"]
            uploadPhoto("http://47.112.187.226/api/update_image.php", image: pickedImage, params: parm, header: [:]) {
                
            }
        }
        
        picker.dismiss(animated:true,completion:nil)
    }
        
    func callUpdateProfileAPI() {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        dic.setValue(txtName.text!, forKey: "name")
        dic.setValue(txtMobile.text!, forKey: "mobile")
        dic.setValue(txtPassword.text!, forKey: "password")
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + UPDATE_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: UPDATE_PROFILE, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func callUpdateProfilePicAPI() {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        dic.setValue(txtName.text!, forKey: "name")
        dic.setValue(txtMobile.text!, forKey: "mobile")
        dic.setValue(txtPassword.text!, forKey: "password")
        
        WebParserWS.fetchDataWithURL(url: BASE_URL + UPDATE_PROFILE as NSString, type: .TYPE_POST_RAWDATA, ServiceName: "Profile Pic", bodyObject: dic as AnyObject, delegate: self, isShowProgress: false)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            if result == "true" {
                
                if ServiceName == UPDATE_PROFILE {
                    let dicUserData = dicResponse?.value(forKey: "UserLogin") as? NSDictionary
                    appDelegate.saveCurrentUserData(dic: dicUserData!)
                    appDelegate.dicLoginUserDetails = dicUserData!
                    self.navigationController?.popViewController(animated: true)

                }
                else {
                    let dicUserData = dicResponse?.value(forKey: "UserLogin") as? NSDictionary
                    appDelegate.saveCurrentUserData(dic: dicUserData!)
                    appDelegate.dicLoginUserDetails = dicUserData!
                }
            }
            
        }
    }
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        SVProgressHUD.show()
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "image0", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "ResponseMsg") as? String)
            self.callUpdateProfilePicAPI()
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                SVProgressHUD.dismiss()
                print("upload success result: \(resut)")
            case .failure(let err):
                SVProgressHUD.dismiss()
                print("upload err: \(err)")
            }
        }
    }
}

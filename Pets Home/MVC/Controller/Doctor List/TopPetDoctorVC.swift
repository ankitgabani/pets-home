//
//  TopPetDoctorVC.swift
//  Pets Home
//
//  Created by Gabani King on 06/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage

class TopPetDoctorVC: UIViewController, UITableViewDelegate, UITableViewDataSource,responseDelegate {
    
    @IBOutlet weak var btnDiagnose: UIButton!
    @IBOutlet weak var btnCertification: UIButton!
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var viewFilter: UIView!
    @IBOutlet weak var txtPetCat: UITextField!
    @IBOutlet weak var viewTargetCat: UIView!
    
    
    @IBOutlet weak var viewType: UIView!
    @IBOutlet weak var txtType: UITextField!
    
    let dropDownType = DropDown()
    var arrayType = ["皮肤科","内科"]
    
    let dropDownPetCat = DropDown()
    var arrayPetCat = ["猫","狗"]
    
    var strWhichPet = 1
    var arrDoctorList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewBg.isHidden = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        viewBg.addGestureRecognizer(tap)
        
        viewFilter.clipsToBounds = true
        viewFilter.layer.cornerRadius = 25
        viewFilter.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively
        
        viewFilter.isHidden = true
        viewFilter.slideOut(to: kFTAnimationBottom, in: viewFilter.superview, duration: 0.0, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        tblView.delegate = self
        tblView.dataSource = self
        
        setdropDownType()
        setDropDownCat()
        
        callDoctorListDataAPI(pid: "", skill: "")
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.tabBarController?.tabBar.isHidden = false
        self.viewBg.isHidden = true
        viewFilter.slideOut(to: kFTAnimationBottom, in: viewFilter.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    func setdropDownType() {
        dropDownType.dataSource = arrayType
        dropDownType.anchorView = viewType
        dropDownType.direction = .any
        dropDownType.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtType.text = item
        }
        dropDownType.bottomOffset = CGPoint(x: 0, y: viewType.bounds.height)
        dropDownType.topOffset = CGPoint(x: 0, y: -viewType.bounds.height)
        dropDownType.dismissMode = .onTap
        dropDownType.textColor = UIColor.darkGray
        dropDownType.backgroundColor = UIColor.white
        dropDownType.selectionBackgroundColor = UIColor.clear
        dropDownType.reloadAllComponents()
    }
    
    func setDropDownCat() {
        dropDownPetCat.dataSource = arrayPetCat
        dropDownPetCat.anchorView = viewTargetCat
        dropDownPetCat.direction = .any
        dropDownPetCat.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtPetCat.text = item
            self.strWhichPet = index + 1
        }
        dropDownPetCat.bottomOffset = CGPoint(x: 0, y: viewTargetCat.bounds.height)
        dropDownPetCat.topOffset = CGPoint(x: 0, y: -viewTargetCat.bounds.height)
        dropDownPetCat.dismissMode = .onTap
        dropDownPetCat.textColor = UIColor.darkGray
        dropDownPetCat.backgroundColor = UIColor.white
        dropDownPetCat.selectionBackgroundColor = UIColor.clear
        dropDownPetCat.reloadAllComponents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDoctorList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TopPetDoctorCell") as! TopPetDoctorCell

        let dicData = arrDoctorList[indexPath.row] as? NSDictionary
        
        let name = dicData?.value(forKey: "doctor_name") as? String
        let skill = dicData?.value(forKey: "skill") as? String
        let rate = dicData?.value(forKey: "rate") as? Int
        
        cell.lblOwner.text = skill
        cell.lblName.text = name
        cell.lblRating.text = "\(rate ?? 0)"
        
        if var image1 = (dicData?.value(forKey: "doctor_pic") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            cell.imgPetName.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgPetName.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            cell.imgPetName.image = UIImage(named: "ic_placeholder")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicData = arrDoctorList[indexPath.row] as? NSDictionary

        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CCertificationViewController = mainStoryboard.instantiateViewController(withIdentifier: "CCertificationViewController") as! CCertificationViewController
        vc.dicPetDetails = dicData!
        vc.isFromDr = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clickedCertification(_ sender: Any) {
        self.tabBarController?.tabBar.isHidden = false
        callDoctorListDataAPI(pid: "", skill: "")

        btnDiagnose.setTitleColor(UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1), for: .normal)
        btnDiagnose.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        
        btnCertification.setTitleColor(UIColor.white, for: .normal)
        btnCertification.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
    }
    
    @IBAction func clickedDiagnosw(_ sender: Any) {

        self.tabBarController?.tabBar.isHidden = true
        self.viewBg.isHidden = false
        viewFilter.isHidden = false
        viewFilter.slideIn(from: kFTAnimationBottom, in: viewFilter.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
        
        btnCertification.setTitleColor(UIColor(red: 57/255, green: 83/255, blue: 149/255, alpha: 1), for: .normal)
        btnCertification.backgroundColor = UIColor(red: 216/255, green: 216/255, blue: 216/255, alpha: 1)
        
        btnDiagnose.setTitleColor(UIColor.white, for: .normal)
        btnDiagnose.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
    }
    
    @IBAction func clickedPetCat(_ sender: Any) {
        dropDownPetCat.show()
    }
    
    @IBAction func clickedType(_ sender: Any) {
        dropDownType.show()
    }
    
    @IBAction func clickedSubmitFilter(_ sender: Any) {
        
        callDoctorListDataAPI(pid: "\(strWhichPet)", skill: txtType.text!)
        
        self.tabBarController?.tabBar.isHidden = false
        self.viewBg.isHidden = true
        
        viewFilter.slideOut(to: kFTAnimationBottom, in: viewFilter.superview, duration: 0.4, delegate: self, start: Selector("temp"), stop: Selector("temp"))
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedDrTab(_ sender: Any) {
    }
    
    @IBAction func clickedServiceTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ArticleViewController = mainStoryboard.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ChattingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChattingsViewController") as! ChattingsViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedAccountTab(_ sender: Any) {
        let type = appDelegate.dicLoginUserDetails.value(forKey: "type") as? String
        
        if type == "医生" {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: DAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "DAccountVC") as! DAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: CAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "CAccountVC") as! CAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
    
    func callDoctorListDataAPI(pid: String,skill: String) {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        dic.setValue(pid, forKey: "pid")
        dic.setValue(skill, forKey: "skill")
        print(dic)
        WebParserWS.fetchDataWithURL(url: BASE_URL + DOCTOR_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: DOCTOR_LIST, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            let result = dicResponse?.value(forKey: "Result") as? String
            let responseMsg = dicResponse?.value(forKey: "ResponseMsg") as? String
            
            if result == "true" {
                
                if ServiceName == DOCTOR_LIST {
                    let arrPetlist = dicResponse?.value(forKey: "DoctorList") as? NSArray
                    
                    self.arrDoctorList.removeAllObjects()
                    self.arrDoctorList = (arrPetlist?.mutableCopy() as? NSMutableArray)!
                    self.tblView.reloadData()
                }
            }
        }
    }
}

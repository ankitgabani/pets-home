//
//  AddArticleVC.swift
//  宠物之家
//
//  Created by Gabani King on 05/03/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD

class AddArticleVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var txtTitle: UITextField!
    
    @IBOutlet weak var txtDetails: UITextView!
    
    var isUploadFile = false
    var selectedImage = UIImage()
    
    let Imgpicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Imgpicker.delegate = self
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedFile(_ sender: Any) {
        Imgpicker.sourceType = .photoLibrary
        Imgpicker.allowsEditing = true
        present(Imgpicker, animated: true, completion: nil)
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func clickedSubmit(_ sender: Any) {
        
        if self.txtTitle.text == "" {
            self.view.hideToast()
            self.view.makeToast("Enter tile")
        }
        else if txtDetails.text == ""
        {
            self.view.hideToast()
            self.view.makeToast("Enter description")
        }
        else if isUploadFile == true
        {
            self.view.hideToast()
            
            let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
            
            let parm = ["uid":user_Id ?? "","size":"1","title":self.txtTitle.text!,"description":self.txtDetails.text!]
            
            uploadPhoto("http://47.112.187.226/api/add_article.php", image: selectedImage, params: parm, header: [:]) {
            }
        }
        else {
            self.view.hideToast()
            callAddArticleAPI()
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.editedImage] as? UIImage {
            
            self.selectedImage = pickedImage
            self.isUploadFile = true
        }
        
        picker.dismiss(animated:true,completion:nil)
    }
    
    func callAddArticleAPI() {
        
        APIClient.sharedInstance.showIndicator()
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let param = ["uid": "\(user_Id ?? "")","title":self.txtTitle.text!,"description":self.txtDetails.text!,"size":"1","image0": ""]
        
        print(param)
        APIClient.sharedInstance.MakeAPICallWithOutHeaderPost(ADD_ARTICLE, parameters: param, completionHandler: { (response, error, statusCode) in
            
            if error == nil {
                print("STATUS CODE \(String(describing: statusCode))")
                print("Response \(String(describing: response))")
                
                let message = response?["ResponseMsg"] as? String ?? ""
                
                if statusCode == 200 {
                    
                    APIClient.sharedInstance.hideIndicator()
                    
                    if let responseUser = response {
                        
                        let strMsg = responseUser.value(forKey: "ResponseMsg") as? String
                        let status = responseUser.value(forKey: "Result") as? String
                        
                        if status == "true" {
                            
                            self.txtDetails.text = ""
                            self.txtTitle.text = ""
                            self.view.makeToast(strMsg)
                        }
                        else {
                            self.view.makeToast(strMsg)
                        }
                        
                    } else {
                        self.view.makeToast(message)
                    }
                    
                } else {
                    
                    APIClient.sharedInstance.hideIndicator()
                    self.view.makeToast(message)
                    
                }
                
            } else {
                
                APIClient.sharedInstance.hideIndicator()
                
                print("Response \(String(describing: response))")
                let message = response?["message"] as? String ?? ""
                AppUtilites.showAlert(title: message, message: "", cancelButtonTitle: "OK")
            }
        })
    }
    
    
    func uploadPhoto(_ url: String, image: UIImage, params: [String : Any], header: [String:String], completion: @escaping () -> ()) {
        
        SVProgressHUD.show()
        let httpHeaders = HTTPHeaders(header)
        AF.upload(multipartFormData: { multiPart in
            for p in params {
                multiPart.append("\(p.value)".data(using: String.Encoding.utf8)!, withName: p.key)
            }
            multiPart.append(image.jpegData(compressionQuality: 0.4)!, withName: "image0", fileName: "file.jpg", mimeType: "image/jpg")
        }, to: url, method: .post, headers: httpHeaders) .uploadProgress(queue: .main, closure: { progress in
            print("Upload Progress: \(progress.fractionCompleted)")
        }).responseJSON(completionHandler: { data in
            print("upload finished: \(data)")
            let responseDict = ((data.value as AnyObject) as? NSDictionary)
            
            self.view.makeToast(responseDict?.value(forKey: "ResponseMsg") as? String)
            print(responseDict?.value(forKey: "ResponseMsg") as? String ?? "")
            self.txtDetails.text = ""
            self.txtTitle.text = ""
            self.isUploadFile = false
            
        }).response { (response) in
            switch response.result {
            case .success(let resut):
                SVProgressHUD.dismiss()
                print("upload success result: \(resut)")
            case .failure(let err):
                SVProgressHUD.dismiss()
                print("upload err: \(err)")
            }
        }
    }
}


//
//  ArticleViewController.swift
//  Pets Home
//
//  Created by Gabani King on 08/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleViewController: UIViewController,UITableViewDelegate, UITableViewDataSource,responseDelegate {
    
    @IBOutlet weak var tblView: UITableView!
    
    var arrArticList = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblView.delegate = self
        tblView.dataSource = self
        
        
        callArticleListDataAPI()
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
        }
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrArticList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell") as! ArticleCell
        
        let dicData = arrArticList[indexPath.row] as? NSDictionary
        
        let title = dicData?.value(forKey: "title") as? String
        let username = dicData?.value(forKey: "username") as? String
        
        cell.lblSub.text = username
        cell.lblName.text = title
        
        if var image1 = (dicData?.value(forKey: "image") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            cell.imgPet.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.imgPet.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            cell.imgPet.image = UIImage(named: "ic_placeholder")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dicPetData = arrArticList[indexPath.row] as? NSDictionary
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ArticleDetailsVc = mainStoryboard.instantiateViewController(withIdentifier: "ArticleDetailsVc") as! ArticleDetailsVc
        vc.dicDetails = dicPetData!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func callArticleListDataAPI() {
        
        let user_Id = appDelegate.dicLoginUserDetails.value(forKey: "id") as? String
        
        let dic = NSMutableDictionary()
        dic.setValue(user_Id, forKey: "uid")
        
        print(dic)
        WebParserWS.fetchDataWithURL(url: BASE_URL + ARTICLE_LIST as NSString, type: .TYPE_POST_RAWDATA, ServiceName: ARTICLE_LIST, bodyObject: dic as AnyObject, delegate: self, isShowProgress: true)
    }
    
    func didFinishWithSuccess(ServiceName: String, Response: AnyObject){
        
        print(Response)
        DispatchQueue.main.async {
            
            let dicResponse = Response as? NSDictionary
            
            if ServiceName == ARTICLE_LIST {
                let arrPetlist = dicResponse?.value(forKey: "ArticleData") as? NSArray
                
                self.arrArticList.removeAllObjects()
                self.arrArticList = (arrPetlist?.mutableCopy() as? NSMutableArray)!
                self.tblView.reloadData()
            }
            
        }
    }
    
    @IBAction func clickedHomeTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: CHomeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CHomeViewController") as! CHomeViewController
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedDrTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: TopPetDoctorVC = mainStoryboard.instantiateViewController(withIdentifier: "TopPetDoctorVC") as! TopPetDoctorVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func clickedServiceTab(_ sender: Any) {
    }
    
    @IBAction func clickedChatTab(_ sender: Any) {
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ChattingsViewController = mainStoryboard.instantiateViewController(withIdentifier: "ChattingsViewController") as! ChattingsViewController
        self.navigationController?.pushViewController(vc, animated: false)
        
    }
    
    @IBAction func clickedAccountTab(_ sender: Any) {
        let type = appDelegate.dicLoginUserDetails.value(forKey: "type") as? String
        
        if type == "医生" {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: DAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "DAccountVC") as! DAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
        else {
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc: CAccountVC = mainStoryboard.instantiateViewController(withIdentifier: "CAccountVC") as! CAccountVC
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}

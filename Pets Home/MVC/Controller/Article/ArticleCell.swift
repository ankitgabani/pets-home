//
//  ArticleCell.swift
//  Pets Home
//
//  Created by Gabani King on 26/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {

    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblSub: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ArticleDetailsVc.swift
//  Pets Home
//
//  Created by Gabani King on 26/02/21.
//  Copyright © 2021 Ankit Gabani. All rights reserved.
//

import UIKit
import SDWebImage

class ArticleDetailsVc: UIViewController {
    
    @IBOutlet weak var imgPet: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgPro: UIImageView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblType: UILabel!
    
    @IBOutlet weak var txtDes: UITextView!
    
    var dicDetails = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let type = dicDetails.value(forKey: "type") as? String
        let username = dicDetails.value(forKey: "username") as? String
        let datetime = dicDetails.value(forKey: "datetime") as? String
        let description = dicDetails.value(forKey: "description") as? String
        
        lblUserName.text = username
        lblDate.text = datetime
        lblType.text = type
        //txtDes.text = description?.htmlToString
        
        let encodedData = description!.data(using: String.Encoding.utf8)!
        var attributedString: NSAttributedString!

        do {
            attributedString = try NSAttributedString(data: encodedData, options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html,NSAttributedString.DocumentReadingOptionKey.characterEncoding:NSNumber(value: String.Encoding.utf8.rawValue)], documentAttributes: nil)
        } catch let error as NSError {
            print(error.localizedDescription)
        } catch {
            print("error")
        }
        
        txtDes.attributedText = attributedString
        
        if var image1 = (dicDetails.value(forKey: "image") as? String) {
            image1 = image1.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image1)")
            imgPet.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgPet.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            imgPet.image = UIImage(named: "ic_placeholder")
        }
        
        if var image2 = (dicDetails.value(forKey: "profile_pic") as? String) {
            image2 = image2.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            let url1 = URL(string: "http://47.112.187.226/\(image2)")
            imgPro.sd_imageIndicator = SDWebImageActivityIndicator.gray
            imgPro.sd_setImage(with: url1, placeholderImage: UIImage(named: "ic_placeholder"))
        }
        else {
            imgPro.image = UIImage(named: "ic_placeholder")
        }
        
        if #available(iOS 13.0, *) {
                   let app = UIApplication.shared
                   let statusBarHeight: CGFloat = app.statusBarFrame.size.height
                   
                   let statusbarView = UIView()
                   statusbarView.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
                   view.addSubview(statusbarView)
                   
                   statusbarView.translatesAutoresizingMaskIntoConstraints = false
                   statusbarView.heightAnchor
                       .constraint(equalToConstant: statusBarHeight).isActive = true
                   statusbarView.widthAnchor
                       .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
                   statusbarView.topAnchor
                       .constraint(equalTo: view.topAnchor).isActive = true
                   statusbarView.centerXAnchor
                       .constraint(equalTo: view.centerXAnchor).isActive = true
                   
               } else {
                   let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
                   statusBar?.backgroundColor = UIColor(red: 92/255, green: 136/255, blue: 243/255, alpha: 1)
               }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func clickedBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options:
                [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
